package udep.ing.poo.miprimeraap.entidades;

public class Usuario {

    private String usuario;
    private String contrasena;

    public Usuario(String usuario, String contrasena) {
        this.usuario = usuario;
        this.contrasena = contrasena;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public boolean equals(Usuario u) {
        return this.usuario.equals(u.getUsuario()) && this.contrasena.equals(u.getContrasena());
    }
}
