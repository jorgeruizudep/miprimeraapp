package udep.ing.poo.miprimeraap;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import udep.ing.poo.miprimeraap.entidades.GestionUsuarios;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editUsuario, editContrasena;
    private Button botonLogin;
    private TextView passwordAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editUsuario = (EditText)findViewById(R.id.editUsuario);
        editContrasena = (EditText)findViewById(R.id.editContrasena);
        botonLogin = (Button)findViewById(R.id.buttonLogin);
        passwordAlert = (TextView)findViewById(R.id.textPasswordAlert);

        botonLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        String eUsuario = editUsuario.getText().toString();
        String eContrasena = editContrasena.getText().toString();

        GestionUsuarios gUsuarios = new GestionUsuarios();

        if (view.getId() == R.id.buttonLogin) {

            if (eUsuario.equals("") || eContrasena.equals("")) {

                passwordAlert.setText("Debe ingresar usuario y contraseña");

            }

            else if (gUsuarios.validarUsuario(eUsuario,eContrasena)) {

                Intent i = new Intent(this,HomeActivity.class);
                i.putExtra("usuario",eUsuario);
                startActivity(i);

            } else {

                passwordAlert.setText("Usuario o contraseña incorrectos");
                editContrasena.setText("");

            }

        }

    }



}