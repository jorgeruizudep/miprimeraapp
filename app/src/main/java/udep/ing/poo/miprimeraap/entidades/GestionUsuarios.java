package udep.ing.poo.miprimeraap.entidades;

import java.util.LinkedList;
import java.util.List;

public class GestionUsuarios {

    private List<Usuario> usuarios;

    public GestionUsuarios() {

        String[] nombres = {"jorge","andre","eduardo","jeanpierre"};
        String[] contrasenas = {"j1234","a1234","e1234","j1234"};

        usuarios = new LinkedList<Usuario>();

        for (int i=0; i<nombres.length; i++) {
            usuarios.add(new Usuario(nombres[i],contrasenas[i]));
        }

    }

    public boolean validarUsuario(Usuario usuario) {

        boolean e = false;

        for (Usuario u : usuarios) {
            e = u.equals(usuario);
            if (e) break;
        }

        return e;
    }

    public boolean validarUsuario(String usuario, String contrasena) {

        return validarUsuario(new Usuario(usuario,contrasena));

    }

}
