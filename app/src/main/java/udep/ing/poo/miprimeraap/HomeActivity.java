package udep.ing.poo.miprimeraap;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {

    TextView textHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        textHome = (TextView)findViewById(R.id.textHome);

        Bundle bundle = getIntent().getExtras();
        String usuario = (String)bundle.get("usuario");
        textHome.setText("Bienvenido " + usuario + "!");

    }
}